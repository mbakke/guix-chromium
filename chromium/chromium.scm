;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2016, 2017, 2018, 2019, 2020 Marius Bakke <mbakke@fastmail.com>
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (chromium chromium)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages chromium)
  #:use-module (chromium widevine)
  #:use-module (ice-9 match))

(define %preserved-third-party-files
  (append
   '("base/third_party/cityhash_v103" ;Expat
     "net/third_party/quiche" ;BSD-3
     "third_party/devtools-frontend/src/front_end/third_party/acorn-logical-assignment" ;Expat
     "third_party/devtools-frontend/src/front_end/third_party/acorn-loose" ;Expat
     "third_party/devtools-frontend/src/front_end/third_party/acorn-numeric-separator" ;Expat
     "third_party/devtools-frontend/src/front_end/third_party/codemirror.next" ;Expat
     "third_party/opencv" ;BSD-3
     "third_party/tflite_support" ;ASL2.0
     "third_party/skia/third_party/vulkanmemoryallocator" ;BSD-3
     "third_party/webrtc_overrides") ;BSD-3
   (@@ (gnu packages chromium) %preserved-third-party-files)))

(define (origin-file origin file)
  "Return a derivation for a soft link that leads to FILE in ORIGIN."
  (computed-file (basename file)
                 #~(symlink (string-append #$origin #$file)
                            #$output)))

(define %ungoogled-origin
  (@@ (gnu packages chromium) %ungoogled-origin))

(define %ungoogled-patches
  ;; Remove build-time dependency on the non-free UnRAR program, and disable
  ;; the "safe browsing" feature that uses it.
  (list (origin-file %ungoogled-origin
                     "/patches/core/debian/disable/unrar.patch")
        (origin-file
         %ungoogled-origin
         "/patches/core/inox-patchset/0001-fix-building-without-safebrowsing.patch")
        ;; The safe browsing patch does some collateral damage.
        (origin-file %ungoogled-origin
                     "/patches/core/ungoogled-chromium/disable-signin.patch")
        (origin-file
         %ungoogled-origin
         "/patches/core/ungoogled-chromium/fix-building-without-safebrowsing.patch")
        (origin-file
         %ungoogled-origin
         "/patches/core/ungoogled-chromium/remove-unused-preferences-fields.patch")
        ;; Handle removal of the bundled (compiled!) 'eu-strip'.
        (origin-file
         %ungoogled-origin
         "/patches/core/ungoogled-chromium/fix-building-with-prunned-binaries.patch")
        ;; Disable warning about missing API keys.
        (origin-file %ungoogled-origin
                     "/patches/extra/debian/disable/google-api-warning.patch")))

(define %debian-patches
  (or (false-if-exception (@@ (gnu packages chromium) %debian-patches))
      '()))

(define %guix-patches
  (or (false-if-exception (@@ (gnu packages chromium) %guix-patches))
      '()))

(define %gcc-patches
  (or (false-if-exception (@@ (gnu packages chromium) %gcc-patches))
      '()))

(define %reverse-patches
  (or (false-if-exception (@@ (gnu packages chromium) %reverse-patches))
      '()))

(define (make-chromium-snippet preserved-files)
  #~(begin
      ;; These patches are "reversed", so handle it in the snippet.
      (for-each (lambda (patch)
                  (invoke #+(file-append patch "/bin/patch")
                          "-Rp1" "-F3" "--force" "--no-backup-if-mismatch"
                          "--input" patch))
                '#$%reverse-patches)

      ;; Prepend file names with "./" for comparison with ftw.
      (let ((preserved-club (map (cut string-append "./" <>)
                                 '#$preserved-files)))

        (define (empty? dir)
          (equal? (scandir dir) '("." "..")))

        (define (third-party? file)
          (string-contains file "/third_party/"))

        (define (useless? file)
          (any (cute string-suffix? <> file)
               '(".tar.gz" ".zip" ".exe" ".jar")))

        (define (parents child)
          ;; Return all parent directories of CHILD up to and including the
          ;; closest "third_party".
          (let* ((dirs (match (string-split child #\/)
                         ((dirs ... last) dirs)))
                 (closest (list-index (lambda (dir)
                                        (string=? "third_party" dir))
                                      (reverse dirs)))
                 (delim (- (length dirs) closest)))
            (fold (lambda (dir prev)
                    (cons (string-append (car prev) "/" dir)
                          prev))
                  (list (string-join (list-head dirs delim) "/"))
                  (list-tail dirs delim))))

        (define (delete-unwanted-files child stat flag base level)
          (let ((protected (make-regexp "\\.(gn|gyp)i?$")))
            (match flag
              ((or 'regular 'symlink 'stale-symlink)
               (when (third-party? child)
                 (unless (or (member child preserved-club)
                             (any (cute member <> preserved-club)
                                  (parents child))
                             (regexp-exec protected child))
                   (format (current-error-port) "deleting ~s~%" child)
                   (force-output)
                   (delete-file child)))
               (when (and (useless? child) (file-exists? child))
                 (delete-file child))
               #t)
              ('directory-processed
               (when (empty? child)
                 (rmdir child))
               #t)
              (_ #t))))

        (nftw "." delete-unwanted-files 'depth 'physical)

        ;; Replace "GN" files from third_party with shims for building
        ;; against system libraries.  Keep this list in sync with
        ;; "build/linux/unbundle/replace_gn_files.py".
        (for-each (match-lambda
                    ((source . destination)
                     (copy-file (string-append "build/linux/unbundle/"
                                               source)
                                destination)))
                  (list
                   '("ffmpeg.gn" . "third_party/ffmpeg/BUILD.gn")
                   '("flac.gn" . "third_party/flac/BUILD.gn")
                   '("fontconfig.gn" . "third_party/fontconfig/BUILD.gn")
                   '("freetype.gn" . "build/config/freetype/freetype.gni")
                   '("harfbuzz-ng.gn" .
                     "third_party/harfbuzz-ng/harfbuzz.gni")
                   '("icu.gn" . "third_party/icu/BUILD.gn")
                   '("libdrm.gn" . "third_party/libdrm/BUILD.gn")
                   '("libevent.gn" . "third_party/libevent/BUILD.gn")
                   '("libjpeg.gn" . "third_party/libjpeg.gni")
                   '("libpng.gn" . "third_party/libpng/BUILD.gn")
                   ;;'("libvpx.gn" . "third_party/libvpx/BUILD.gn")
                   '("libwebp.gn" . "third_party/libwebp/BUILD.gn")
                   '("libxml.gn" . "third_party/libxml/BUILD.gn")
                   '("libxslt.gn" . "third_party/libxslt/BUILD.gn")
                   '("openh264.gn" . "third_party/openh264/BUILD.gn")
                   '("opus.gn" . "third_party/opus/BUILD.gn")
                   '("re2.gn" . "third_party/re2/BUILD.gn")
                   ;;'("snappy.gn" . "third_party/snappy/BUILD.gn")
                   '("zlib.gn" . "third_party/zlib/BUILD.gn")))

        ;; Allow using the unbundled libraries even for "official" builds
        ;; (it controls optimization levels, not branding!).
        (substitute* "tools/generate_shim_headers/generate_shim_headers.py"
          (("#if defined\\(OFFICIAL_BUILD\\)")
           "#if defined(GOOGLE_CHROME_BUILD)")))))

(define (enable-widevine chromium)
  (package
    (inherit chromium)
    (arguments
     (substitute-keyword-arguments (package-arguments chromium)
       ((#:configure-flags flags)
        ;; Enable support for loading the Widevine DRM library.
        #~(cons "enable_widevine=true"
                (delete "enable_widevine=false" #$flags)))))))

(define-public chromium
  (package
    (inherit ungoogled-chromium)
    (name "chromium")
    (version (match (string-split (package-version ungoogled-chromium) #\-)
               ((version revision) version)))
    (home-page "https://www.chromium.org/")
    (source (origin
              (inherit (package-source ungoogled-chromium))
              (patches (append %debian-patches
                               %ungoogled-patches
                               %guix-patches
                               %gcc-patches))
              (modules '((srfi srfi-1)
                         (srfi srfi-26)
                         (ice-9 ftw)
                         (ice-9 match)
                         (ice-9 regex)
                         (guix build utils)))
              (snippet (make-chromium-snippet %preserved-third-party-files))))
    (arguments
     (substitute-keyword-arguments (package-arguments (enable-widevine
                                                       ungoogled-chromium))
       ((#:modules modules)
        `((srfi srfi-1)
          ,@modules))
       ((#:configure-flags flags)
        #~(fold delete #$flags
                '(;; Drop flags that require additional patches.
                  "enable_mdns=false"
                  "enable_remoting=false"
                  "enable_reporting=false"
                  "enable_service_discovery=false")))))
    (description
     "Chromium is a web browser built for speed and security.")))

(define-public chromium+drm
  (make-chromium-widevine chromium widevine))

(define-public ungoogled-chromium+drm
  (make-chromium-widevine (enable-widevine ungoogled-chromium)
                          widevine))
