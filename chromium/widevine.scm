;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Marius Bakke <mbakke@fastmail.com>
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (chromium widevine)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix utils)
  #:use-module (guix build-system trivial)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages elf)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages nss)
  #:use-module (guix i18n)
  #:use-module (guix gexp)
  #:use-module (guix licenses)
  #:export (make-chromium-widevine widevine))

(define (widevine-origin arch version hash)
  (origin
    (method url-fetch)
    (uri (string-append "https://dl.google.com/linux/deb/pool/main/g/"
                        "google-chrome-stable/google-chrome-stable_"
                        version "_" arch ".deb"))
    (sha256 (base32 hash))))

(define widevine-x86_64
  (widevine-origin "amd64" "104.0.5112.79-1"
                   "1m09bbh6a4sm5i0n8z2wy0hb8s7w0c2d335mpyrmndzs45py5ggx"))

(define* (widevine-for-target #:optional
                              (target (or (%current-target-system)
                                          (%current-system))))
  (cond
   ((string-prefix? "x86_64" target) widevine-x86_64)
   (else (error (format #f (G_ "unsupported target: ~a") target)))))

(define-syntax widevinecdm
  (identifier-syntax (widevine-for-target)))

(define (make-chromium-widevine chromium widevine)
  (package
    (name (string-append (package-name chromium) "-widevine"))
    (version (package-version chromium))
    (source #f)
    (native-inputs
     (list sed))
    (inputs
     (list chromium
           (car (assoc-ref (package-inputs chromium) "ffmpeg"))
           widevine))
    (build-system trivial-build-system)
    (arguments
     (list
      #:modules '((guix build utils))
      #:builder
      #~(begin
          (use-modules ((guix build utils)))
          (let ((wrapper (string-append #$output "/bin/chromium")))
            (copy-recursively #$chromium #$output)
            (copy-recursively #$widevine
                              (string-append #$output "/lib"))
            (force-output)
            (make-file-writable wrapper)
            ;; The wrapper script contains an absolute reference to the original
            ;; chromium binary.  Make it reference this package instead so that
            ;; it searches for libwidevinecdm.so in the correct directory.
            ;; FIXME: We use sed here because (substitute* ...) throws
            ;; "Wrong type to apply: <search-regex>"!??!?
            (invoke (search-input-file %build-inputs "bin/sed") "-i"
                    (string-append "s|" #$chromium "|" #$output "|") wrapper)

            ;; XXX: Chromium occasionally complains that libavcodec.so.58 is
            ;; unavailable.  Help it locate the ffmpeg libraries.  This should
            ;; possibly be moved upstream.
            (wrap-program wrapper
              `("LD_LIBRARY_PATH" ":" prefix
                (,(dirname (search-input-file %build-inputs
                                              "/lib/libavcodec.so")))))))))
    (home-page (package-home-page chromium))
    (synopsis (string-append (package-synopsis chromium) " (with Widevine DRM)"))
    (description (package-description chromium))
    (license (cons (non-copyleft
                    "https://chromium.googlesource.com/chromium/src/+/refs/heads/\
master/third_party/widevine/LICENSE"
                    "This is a proprietary license; see the LICENSE file.")
                   (package-license chromium)))))

(define widevine
  (with-imported-modules '((guix build utils))
    (computed-file "widevine"
                   #~(begin
                       (use-modules ((guix build utils)))
                       (set-path-environment-variable
                        "PATH" '("bin")
                        (list #+(canonical-package binutils)
                              #+(canonical-package tar)
                              #+(canonical-package xz)
                              #+(canonical-package patchelf)))
                       (mkdir #$output)
                       (invoke "ar" "x" #+widevinecdm)
                       (invoke "tar" "-xf" "data.tar.xz" "--strip-components=4"
                               "-C" #$output "./opt/google/chrome/WidevineCdm")
                       (invoke "patchelf" "--set-rpath"
                               (string-append #$glib "/lib:"
                                              #$nss "/lib/nss:"
                                              #$nspr "/lib:"
                                              #$gcc:lib "/lib")
                               (string-append #$output "/WidevineCdm/_platform_specific"
                                              "/linux_x64/libwidevinecdm.so"))))))
